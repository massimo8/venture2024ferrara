# SUPPORT

## Big picture
Il sito di un evento IAM, come ad esempio un Agile Venture, è di fatto un sito statico (HTML, CSS, JS).  
Per poterci lavorare bisogna avere accesso a un repository creato ad hoc.  
Una volta ottenuto l'accesso, è possibile effettuare tutte le modifiche desiderate.  
Per pubblicare il sito, basta eseguire un `git push` del branch principale: una pipeline di deploy provvederà 
a trasferire tutti i contenuti via FTP.  


## Esempi di siti già pubblicati in passato
Qui qualche esempio da cui si può prendere spunto: 
- [Agile Venture Vimercate 2020](https://www.agilemovement.it/venture/2020/vimercate/)
- [Torino Agile Conference 2018](https://www.agilemovement.it/venture/2018/torino/)
- [Agile Venture Milano 2020](https://www.agilemovement.it/venture/2020/milano/)
- [Working Software Conference](https://www.agilemovement.it/workingsoftware/)
- [Agile People Camp](https://www.agilemovement.it/agilepeoplecamp/)


## Loghi e grafiche
Alcuni esempi di loghi sono presenti nella cartella [asset](asset).  
Si raccomanda di non modificare il logo, se non per quanto riguarda la scritta della città ospitante.  


## Gestione della Call for Speakers
Per la C4S suggeriamo [Sessionize](https://sessionize.com/)


