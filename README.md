# Agile Venture Ferrara 2024

[![pipeline status](https://gitlab.com/italian-agile-movement/venture2024ferrara/badges/master/pipeline.svg)](https://gitlab.com/italian-agile-movement/venture2024ferrara/commits/master)

Sito Agile Venture Ferrara, ... 2024  
[https://agilemovement.it/venture/2024/ferrara/](https://agilemovement.it/venture/2024/ferrara/)


--- 
## Run locally
You can use _Node.js_ to run the website locally.

~~~ sh
$ npm install
$ npm start
~~~

## Support
See [SUPPORT.md](SUPPORT.md) file.  